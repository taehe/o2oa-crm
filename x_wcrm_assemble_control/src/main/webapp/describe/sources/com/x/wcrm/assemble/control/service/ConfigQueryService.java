package com.x.wcrm.assemble.control.service;

import com.x.base.core.container.EntityManagerContainer;
import com.x.base.core.container.factory.EntityManagerContainerFactory;
import com.x.base.core.project.http.EffectivePerson;
import com.x.base.core.project.tools.ListTools;
import com.x.wcrm.core.entity.WCrmConfig;
import com.x.wcrm.core.entity.tools.filter.QueryFilter;
import com.x.wcrm.core.entity.tools.filter.term.EqualsTerm;
import com.x.wcrm.core.entity.tools.filter.term.InTerm;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 对配置信息查询的服务
 * 
 * @author O2LJ
 */
public class ConfigQueryService {

	private ConfigService configService = new ConfigService();
	
	/**
	 * 根据配置的标识查询项目信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public WCrmConfig get(String id ) throws Exception {
		if ( StringUtils.isEmpty( id )) {
			return null;
		}
		try (EntityManagerContainer emc = EntityManagerContainerFactory.instance().create()) {
			return configService.get(emc, id );
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * 根据ID列表查询项目模板信息列表
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public List<WCrmConfig> list(List<String> ids) throws Exception {
		if (ListTools.isEmpty( ids )) {
			return null;
		}
		try (EntityManagerContainer emc = EntityManagerContainerFactory.instance().create()) {
			return emc.list( WCrmConfig.class,  ids );
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * 根据项目ID列表查询项目信息列表，根据上一条的sequnce查询指定数量的信息
	 * @param pageSize
	 * @param lastId
	 * @param orderField
	 * @param orderType
	 * @param projectIds
	 * @return
	 * @throws Exception
	 */
	public List<WCrmConfig> listWithConfigIdFilter( Integer pageSize, String lastId, String orderField, String orderType, List<String> projectIds ) throws Exception {
		WCrmConfig project = null;
		if( pageSize == 0 ) { pageSize = 20; }
		if( StringUtils.isEmpty( orderField ) ) { 
			orderField = "createTime";
		}
		if( StringUtils.isEmpty( orderType ) ) { 
			orderType = "desc";
		}
		QueryFilter queryFilter = new QueryFilter();
		queryFilter.addInTerm( new InTerm("id", new ArrayList<>(projectIds) ));
		try (EntityManagerContainer emc = EntityManagerContainerFactory.instance().create()) {
			if( lastId != null ) {
				project = emc.find( lastId, WCrmConfig.class );
			}
			if( project != null ) {
				return configService.listWithFilter(emc, pageSize, project.getSequence(), orderField, orderType, null, null, null, null, queryFilter );
			}else {
				return configService.listWithFilter(emc, pageSize, null, orderField, orderType, null, null, null, null, queryFilter );
			}	
		} catch (Exception e) {
			throw e;
		}
	}


	/**
	 * 根据条件查询配置ID列表，最大查询2000条,查询未删除
	 * @param effectivePerson
	 * @param queryFilter
	 * @return
	 * @throws Exception 
	 */
	public List<String> listAllConfigIds(EffectivePerson effectivePerson, int maxCount, QueryFilter queryFilter) throws Exception {
		String personName = effectivePerson.getDistinguishedName();
		if( maxCount ==  0) {
			maxCount = 1000;
		}
		try (EntityManagerContainer emc = EntityManagerContainerFactory.instance().create()) {
			/*queryFilter.addEqualsTerm( new EqualsTerm( "deleted", false ) );*/
			return configService.listAllConfigIds( emc, maxCount, personName, queryFilter );
		} catch (Exception e) {
			throw e;
		}
	}
}
