package com.x.wcrm.assemble.control.jaxrs.config;

import com.x.base.core.container.EntityManagerContainer;
import com.x.base.core.container.factory.EntityManagerContainerFactory;
import com.x.base.core.entity.JpaObject;
import com.x.base.core.project.bean.WrapCopier;
import com.x.base.core.project.bean.WrapCopierFactory;
import com.x.base.core.project.exception.ExceptionEntityNotExist;
import com.x.base.core.project.http.ActionResult;
import com.x.base.core.project.http.EffectivePerson;
import com.x.wcrm.assemble.control.Business;
import com.x.wcrm.core.entity.WCrmConfig;

class ActionGet extends BaseAction {

	ActionResult<Wo> execute(EffectivePerson effectivePerson, String id) throws Exception {
		try (EntityManagerContainer emc = EntityManagerContainerFactory.instance().create()) {
			ActionResult<Wo> result = new ActionResult<>();
			Business business = new Business(emc);
			WCrmConfig config = emc.find(id, WCrmConfig.class);
			if (null == config) {
				throw new ExceptionEntityNotExist(id, WCrmConfig.class);
			}
			Wo wo = Wo.copier.copy(config);
			result.setData(wo);
			return result;
		}

	}

	public static class Wo extends WCrmConfig {
		private static final long serialVersionUID = 5661133561098715100L;
		public static WrapCopier<WCrmConfig, Wo> copier = WrapCopierFactory.wo(WCrmConfig.class, Wo.class, null,
				JpaObject.FieldsInvisible);
	}
}
